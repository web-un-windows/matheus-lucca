package main

import (
    "encoding/json"
    "fmt"
    "math"
    "net/http"
    "strconv"
    "strings"
)

func main() {
    RunServer()
}

func RunServer() {
    s := http.Server{
        Addr:    "localhost:8080",
        Handler: Manipulador{},
    }
    s.ListenAndServe()
}

type Manipulador struct{}

func (Manipulador) ServeHTTP(res http.ResponseWriter, req *http.Request) {
    if req.Method != "GET" {
        writeJSONResponse(res, http.StatusBadRequest, ResponseError{Code: http.StatusBadRequest, Error: "Método não suportado"})
        return
    }
    expression := req.URL.Query().Get("op")
    if expression == "" {
        writeJSONResponse(res, http.StatusBadRequest, ResponseError{Code: http.StatusBadRequest, Error: "Operação não fornecida"})
        return
    }
    result, err := processExpression(expression)
    if err != nil {
        writeJSONResponse(res, http.StatusBadRequest, ResponseError{Code: http.StatusBadRequest, Error: err.Error()})
        return
    }

    writeJSONResponse(res, http.StatusOK, ResponseSuccess{Result: result, Op: expression})
}

func processExpression(expression string) (float64, error) {
    parts := strings.Fields(expression)

    operator := parts[1]

    calculator, ok := SupportedOperations[operator]
    if !ok {
        return 0, fmt.Errorf("operador inválido: %s", operator)
    }

    return calculator.Calculate(), nil
}

func calculateResult(op string, number1 float64, number2 float64) (float64, error) {
    parts := strings.Fields(op)
    
    var err error

	number1, err = strconv.ParseFloat(parts[0], 64)
	if err != nil {
	    return 0, fmt.Errorf("número1 inválido: %s", parts[0])
	}

	number2, err = strconv.ParseFloat(parts[2], 64)
	if err != nil {
	    return 0, fmt.Errorf("número2 inválido: %s", parts[2])
	}

    operator := parts[1]

    calculator, ok := SupportedOperations[operator]
    if !ok {
        return 0, fmt.Errorf("operador inválido: %s", operator)
    }

    return calculator.Calculate(), nil
}

func writeJSONResponse(res http.ResponseWriter, statusCode int, data interface{}) {
    res.Header().Set("Content-Type", "application/json")
    res.WriteHeader(statusCode)
    json.NewEncoder(res).Encode(data)
}

type ArithmeticOperation struct {
    Number1  float64 `json:"number1"`
    Number2  float64 `json:"number2"`
    Operator string  `json:"operator"`
}

type Calculator interface {
    Calculate() float64
}

type AddOperation struct {
    ArithmeticOperation
}

func (a AddOperation) Calculate() float64 {
    return a.Number1 + a.Number2
}

type SubOperation struct {
    ArithmeticOperation
}

func (s SubOperation) Calculate() float64 {
    return s.Number1 - s.Number2
}

type MulOperation struct {
    ArithmeticOperation
}

func (m MulOperation) Calculate() float64 {
    return m.Number1 * m.Number2
}

type DivOperation struct {
    ArithmeticOperation
}

func (d DivOperation) Calculate() float64 {
    if d.Number2 == 0 {
        return 0
    }
    return d.Number1 / d.Number2
}

type PowOperation struct {
    ArithmeticOperation
}

func (p PowOperation) Calculate() float64 {
    return math.Pow(p.Number1, p.Number2)
}

type RotOperation struct {
    ArithmeticOperation
}

func (r RotOperation) Calculate() float64 {
    return math.Sqrt(r.Number1)
}

var SupportedOperations = map[string]Calculator{
    "+":   AddOperation{},
    "-":   SubOperation{},
    "*":   MulOperation{},
    "/":   DivOperation{},
    "^":   PowOperation{},
    "&":   RotOperation{},
    "sum": AddOperation{},
    "sub": SubOperation{},
    "mul": MulOperation{},
    "div": DivOperation{},
    "pow": PowOperation{},
    "rot": RotOperation{},
}

type ResponseError struct {
    Code  int    `json:"code"`
    Error string `json:"error"`
}

type ResponseSuccess struct {
    Result float64 `json:"result"`
    Op     string  `json:"op"`
}